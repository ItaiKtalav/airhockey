﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

	public int scorePerGoal = 1;
	private int[] score = new int[2];
	public Text[] scoreText;

	void Start () {    
		// subscribe to events from all the Goals
		Goal[] goals = FindObjectsOfType<Goal> ();

		for (int i = 0; i < goals.Length; i++) {
			goals[i].scoreGoalEvent += OnScoreGoal;
		}

		// reset the scores to zero
		for (int i = 0; i < score.Length; i++) {
			score [i] = 0;
			scoreText [i].text = "0";
		}
	}

	public void OnScoreGoal(int player) {
		// add points to the player whose goal it is
		score[player] += scorePerGoal;
		scoreText[player].text = "Score: "+score[player].ToString ();
		Debug.Log ("Player " + player + ": " + score[player]);

		if (score [player] == 5) {
			print ("winrar");
			scoreText[2].text = "PLAYER "+player.ToString()+" WINS!!!";
			Time.timeScale = 0;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetAxis("Submit") > 0) {
			Time.timeScale = 1;
		}
		print (Input.GetAxis ("Submit"));
	}


	///////
}
