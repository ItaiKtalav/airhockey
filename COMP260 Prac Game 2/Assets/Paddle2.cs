﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class Paddle2 : MonoBehaviour {

	private Rigidbody rigidbody;
	public float force = 10f;
	public PuckControll puck;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	// Update is called once per frame
	void Update () {
		//transform.LookAt (puck.transform);
		rigidbody.AddForce ((transform.position - puck.transform.position) * -2);
	}
		
		
	void FixedUpdate () {

	}
}
