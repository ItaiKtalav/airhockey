﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {

	public LayerMask puckLayer;
	public delegate void ScoreGoalHandler(int player);
	public ScoreGoalHandler scoreGoalEvent;
	public int player;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerStay(Collider collider) {
		// play score sound
		//audio.PlayOneShot(scoreClip);

		// reset the puck to its starting position
		PuckControll puck = collider.gameObject.GetComponent<PuckControll>();
		puck.ResetPosition();

		if (scoreGoalEvent != null) {
			scoreGoalEvent(player);
		}
	}
}
